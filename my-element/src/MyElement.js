import { html, css, LitElement } from 'lit';

export class MyElement extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--my-element-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number },
      user: {type:String},
      age: {type:Number}
    };
  }

  constructor() {
    super();
    this.title = 'Hey there';
    this.counter = 5;
  }

  __increment() {
    ++counter ;
  }

  render() {
    return html`
      <h2>${this.title} Nr. ${this.counter}!</h2>
      <h2>${this.user} Nr. ${this.age}!</h2>
      <button @click=${this.__increment}>increment</button>
    `;
  }
}
