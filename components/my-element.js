import {LitElement, html} from 'lit-element';
import GetMyPostsDm from './get-my-posts-dm';

class MyElement extends LitElement {
  static get properties(){
    return{
      propObj :{type: Object},
      propStr : {type: String},
      users : {type: Array},
      propNum : {type: Number},

    }
  }
  constructor(){
   super();
   this.propObj = {};
   this.propStr = '';
   this.users = [];
   this.propNum = 0;

  }
  __increment() {
    this.propNum++ ;
  }


  _getPosts(){
    let _dmGetPosts = new GetMyPostsDm();
    _dmGetPosts.addEventListener('success-call', this._setPosts.bind(this));
    _dmGetPosts.addEventListener('error-call', this._showModalError.bind(this));
    _dmGetPosts.generateRequest();
  }
  _showModalError(configError){
    this.errorModal = configError;
  }
  _setPosts({detail}){
    this.users = detail;
  }

  render() {
    return html`
      <div>${this.propStr}</div>
      <label>${this.propNum}</label>
      <ul>
        ${this.users.map(({name, age }) =>
          html`<li>${name}</li>
               <li>${age}</li>`
          )}
      </ul>
      <label>${this.propStr.name}</label>
      <label>${this.propNum.age}</label>
      <button @click="${this._getPosts}">Click</button>
      <button @click="${this.__increment}">Number</button>
    `;
  }
}

customElements.define('my-element', MyElement);




